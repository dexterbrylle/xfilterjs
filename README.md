## XFilterJS

A util package for working with JS Arrays


### Installation

```
npm install --save xfilterjs
```

### Usage

``` javascript
const xfilter = require('xfilterjs');

let foo = [1, 2, 3, 'a', 2, 'a', 4, 'b'];

let bar = xfilter.duplicates(foo);

console.log(bar); // Output: [1, 3, 4, b]
```
