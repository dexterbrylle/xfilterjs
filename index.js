/**
 * filterjs
 * @description Util package for working with JS arrays
 * @version 0.1.3
 * @author Dexter Brylle Matos <dexterbrylle@gmail.com>
 */

'use strict';

const xfilter = {};

xfilter.duplicates = function (array) {
	return array.filter(function (item, position) {
		return array.indexOf(item) === position;
	});
}

module.exports = xfilter;
